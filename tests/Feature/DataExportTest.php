<?php

namespace Tests\Feature;

use App\Model\BankLog;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Log;


class DataExportTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testValidUserAndDateGetsZipFile()
    {
        //arrange
        $entry = factory(BankLog::class)->create();
        //action
        $date = $entry->date->format('Y-m-d');
        $user_id = $entry->user_id;
        $response = $this->get('/mock/fetch/' . $user_id . '_' .  $date);
        $expected_zip_name = $user_id . '_' . $date . '.zip';

        //assert
        $this->assertTrue($response->headers->get('content-type') == 'application/octet-stream');
        $this->assertTrue($response->headers->get('content-disposition') == 'attachment; filename=' . $expected_zip_name);

        $response->assertStatus(200);
    }
}
