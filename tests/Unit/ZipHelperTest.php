<?php

namespace Tests\Unit;

use App\Exports\GenericExport;
use App\Helpers\ZipHelper;
use App\Model\BankLog;
use Tests\TestCase;
use Excel;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ZipHelperTest extends TestCase
{
    /**
     * Test if zip file is created from collection
     *
     * @return void
     */
    public function test_zip_file_is_created()
    {
        //arrange

        factory(BankLog::class, 10)->create();
        $selection = ['user_id', 'iban', 'subject', 'amount', 'date'];
        $results = BankLog::Select($selection)->limit(10)->orderByDesc('id');

        $filename = "test.csv";
        $zipFileName = 'test.zip';
        Excel::store(new GenericExport($results, $selection), $filename);

        //action
        $filepath = ZipHelper::GenerateZipFile($filename, $zipFileName);
        //assert
        $status = file_exists($filepath);
        $this->assertTrue($status);
    }
}
