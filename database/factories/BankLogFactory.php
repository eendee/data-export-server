<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use App\Model\BankLog;
use Faker\Generator as Faker;



$factory->define(BankLog::class, function (Faker $faker) {
    return [
        'user_id' => rand(1, 100),
        'iban' => $faker->iban('DE'),
        'amount' => rand(1000, 100000),
        'subject' => $faker->word,
        'date' => $faker->dateTimeThisMonth($max = 'now', $timezone = null, $format = 'Y-m-d')
    ];
});
