<?php

namespace App\Http\Controllers;

use App\Exports\GenericExport;
use App\Helpers\DateHelper;
use App\Helpers\ZipHelper;
use App\Model\BankLog;
use Illuminate\Http\Response;
use Excel;
use \ZipArchive;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ExportController extends Controller
{
    /**
     * Generate zip of csv of user transactions from log.
     *
     * @param  string {userid}_{date} (date in Y-m-d)
     * @return Zip file or error message
     */
    function ExportUserZip($userid_date = null)
    {
        $parameters = $this->VaidateString($userid_date);
        $selection = ['iban', 'subject', 'amount', 'date'];
        $results = BankLog::Where('user_id', $parameters[0])->select($selection);

        $filename = $parameters[0] . '_' . $parameters[1] . ".csv";
        Excel::store(new GenericExport($results, $selection), $filename);
        $zipFileName = $parameters[0] . '_' . $parameters[1] . '.zip';
        try {
            $filepath = ZipHelper::GenerateZipFile($filename, $zipFileName);
            if (file_exists($filepath)) {
                $headers = array('Content-Type' => 'application/octet-stream');
                return response()->download($filepath, $zipFileName, $headers);
            } else {
                return response(array('error' => 'something went wrong serving the file'), Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        } catch (\Throwable $th) {

            Log::error($th->getMessage());
            return response(array('error' => 'failed to generate file'), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Validates the request input string.
     *
     * @param  string {userid}_{date} (date in Y-m-d)
     * @return array array of userid and date or error responses
     */
    private function VaidateString($userid_date)
    {
        if ($userid_date == null) {
            return response(array('error' => 'invalid url format'), Response::HTTP_BAD_REQUEST);
        }
        $parameters = explode('_', $userid_date);
        if (count($parameters) !== 2) {
            return response(array('error' => 'invalid url format'), Response::HTTP_BAD_REQUEST);
        }
        if (is_int($parameters[0])) {
            return response(array('error' => 'invalid url format: user_id must be integer'), Response::HTTP_BAD_REQUEST);
        }
        if (!DateHelper::ValidateDateIsYmdFormat($parameters[1])) {
            return response(array('error' => 'invalid url format: Date must be of format: Y-m-d'), Response::HTTP_BAD_REQUEST);
        }
        return $parameters;
    }
}
