<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BankLog extends Model
{
    public $timestamps = false;
}
