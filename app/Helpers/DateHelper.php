<?php

namespace App\Helpers;

class DateHelper
{
    /**
     * Validate input string as Y-m-D date.
     *
     * @param  string date
     * @return boolean 
     */
    public static  function ValidateDateIsYmdFormat($date)
    {
        $temp = explode('-', $date);
        if (count($temp) !== 3) return false;
        return checkdate($temp[1], $temp[2], $temp[0]);
    }
}
