<?php

namespace App\Helpers;

use \ZipArchive;

class ZipHelper
{

    /**
     * Generate zip file given an input file.
     *
     * @param  string $filename:the name of the file to zip
     * @param  string $zipFileName:the name of the zip file to be generated
     * @throws Exception
     * @return string path to the zip file generated
     */
    public static  function GenerateZipFile($filename, $zipFileName)
    {
        $public_dir = public_path() . '/uploads';
        if (!file_exists($public_dir)) {
            mkdir($public_dir, 0777, true);
        }
        $file_handle = storage_path('app/' . $filename);
        $zip = new ZipArchive;
        if ($zip->open($public_dir . '/' . $zipFileName, ZipArchive::CREATE | ZipArchive::OVERWRITE) === TRUE) {
            $zip->addFile($file_handle, $filename);
            $zip->close();
            $filepath = $public_dir . '/' . $zipFileName;
            return $filepath;
        } else {
            throw new Exception("Error generating the ZIP file " . $zipFileName);
        }
    }
}
